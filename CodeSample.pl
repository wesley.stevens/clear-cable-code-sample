#Quick code sample utilizing Perl to add game enemies to a PostgreSQL database
#Written by: Wesley Stevens
#Sept 10 2019

#!/usr/bin/perl

#List of modules
use strict;
use warnings;
use DBI;


sub connect_to_database
{
    my $driver = "Pg";
    my $database = "encounterdb";
    
    #Postgresql connection information
    my $dsn = "DBI:$driver:dbname = $database;host = 127.0.0.1;port = 5432";
    my $username = "******"; 
    my $password = "******";
    
    #Database handler
    my $dbh = DBI->connect($dsn, $username, $password, { RaiseError => 1 }) 
       or die $DBI::errstr;
    print "Opened database successfully\n";
    return $dbh
}

sub create_table
{
    #Database handler is the first argument
    my $dbh = $_[0];
    my $create_table =  qq(CREATE TABLE IF NOT EXISTS ENEMIES
                          (ID    SERIAL                   ,
                           NAME  TEXT             NOT NULL,
                           LEVEL INT              NOT NULL,
                           HP    INT              NOT NULL););
    #Execute statement
    my $return = $dbh->do($create_table);
    
    #Error handling
    if ($return < 0) 
    {
        print $DBI::errstr;
    }
    else
    {  
        print "Table created\n"
    }
}

sub insert_enemy
{
    #Database handler is the first argument
    my $dbh = $_[0];

    #Get user input for enemy name
    print "Enter the name of the enemy (then press enter)\n";
    my $enemy_name = <STDIN>;
    
    #Clean up user input
    chomp $enemy_name;

    #Get user input for enemy level
    print "Enter the creature level of the enemy (then press enter)\n";
    my $creature_level = <STDIN>;

    #Check if creature level is a number
    while (!($creature_level =~ /^-?\d+$/))
    {
       print "Entry is not an integer, please enter a number (then press enter)\n";  
       $creature_level = <STDIN>;        
    }

    chomp $creature_level;

    #Get user input for enemy hp
    print "Enter the hp of the enemy (then press enter)\n";
    my $enemy_hp = <STDIN>;

    #Check if hp is a number
    while (!($enemy_hp =~ /^-?\d+$/))
    {
       print "Entry is not an integer, please enter a number (then press enter)\n";  
       $enemy_hp = <STDIN>;        
    }

    chomp $enemy_hp;

    #Execute the insert statement
    my $insert_statement = qq(INSERT INTO ENEMIES (NAME, LEVEL, HP) VALUES ('$enemy_name', $creature_level, $enemy_hp););
    my $return = $dbh->do($insert_statement) or die $DBI::errstr; 

    #Error handling
    if ($return < 0) 
    {
        print $DBI::errstr;
    }
    else
    {  
        print "Enemy successfully inserted into the table\n"
    }
}

sub close_database_connection
{
    #Database handler is the first argument
    my $dbh = $_[0];

    #Close connection
    $dbh->disconnect();
}

#Main
my $dbh = connect_to_database();
create_table($dbh);
insert_enemy($dbh);
close_database_connection($dbh);                       